#include <string.h>
#include "opencv2/opencv.hpp"

typedef unsigned short u16;

enum{
    CAM_NUM       = 2,
    IMAGE_SIZE    = 1920*1080*3,
    RING_BUF_SIZE = 1920*1080*3*5
};

class Ring_buffer
{
    public:
    uchar ring_buf[RING_BUF_SIZE];                          // ring buffer
    unsigned int ring_buf_pointer;                          // buffer pointer

    public:
    Ring_buffer():ring_buf_pointer(0)
    {
        memset(ring_buf,0,sizeof(ring_buf));
    }
    void Ring_Buffer_Push(cv::Mat);
    uchar* Ring_Buffer_Pop(void);
};

void Ring_buffer::Ring_Buffer_Push(cv::Mat mat)
{
    memcpy(ring_buf+ring_buf_pointer,mat.data,IMAGE_SIZE);    //링버퍼의 load pointer자리에 데이터 copy
    ring_buf_pointer+=IMAGE_SIZE;                             //load pointer 이미지 크기만큼 증가

    printf("push!\n");
    
    if (ring_buf_pointer == RING_BUF_SIZE) {                  //버퍼 끝에 도달 시 lp 0으로 초기화
        ring_buf_pointer = 0;
    }
}

uchar* Ring_buffer::Ring_Buffer_Pop(void)
{
    uchar *ret = 0;
    // printf("%d\n",ring_buf_pointer);                        //debug용 현재 포인터 출력

    if (ring_buf_pointer >= IMAGE_SIZE) {                      //링버퍼 안에 데이터 있을 시
        ret = &ring_buf[ring_buf_pointer-IMAGE_SIZE];          //ret에 링버퍼에 저장되어있는 최신 데이터의 포인터 저장
        printf("pop! \n");
    }
    else if(ring_buf_pointer == 0)                             //끝에 도달하여 초기 칸으로 포인터가 돌아올 경우              
    {
        if(ring_buf[RING_BUF_SIZE-IMAGE_SIZE] != 0)            //초기에 push없이 pop하는거 방지용
        {
            ret = &ring_buf[RING_BUF_SIZE-IMAGE_SIZE];         //ret에 링버퍼의 마지막에 저장되어있는 데이터의 포인터 저장
            printf("last pop! \n");
        }
    }
    else                                                       //데이터 없을경우 NULL 반환
    {
        printf("no data\n");
        ret=NULL;
    }
    return ret;
}
