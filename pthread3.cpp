#include <string.h>
#include "opencv2/opencv.hpp"
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <vector>

typedef unsigned short u16;
pthread_mutex_t mutx;

enum{
    CAM_NUM       = 2,
    IMAGE_SIZE    = 1920*1080*3,
    RING_BUF_SIZE = 5,
    RING_BUF_END  = RING_BUF_SIZE,
    RING_BUF_START =0
};

class Ring_buffer
{
    public:
    std::vector<cv::Mat> ring_buf;                          // ring buffer
    int ring_buf_pointer;                                   // buffer pointer

    public:
    Ring_buffer():ring_buf_pointer(0)
    {
        ring_buf.reserve(RING_BUF_SIZE);
        for(int i=0;i<RING_BUF_SIZE;i++)
        {
            ring_buf[i].create(1080,1920,CV_8UC3);
        }
    }
    void Ring_Buffer_Push(cv::Mat);
    uchar* Ring_Buffer_Pop(void);
};

void Ring_buffer::Ring_Buffer_Push(cv::Mat mat)
{
    mat.copyTo(ring_buf[ring_buf_pointer]);         //ring buffer에 복사
    ring_buf_pointer++;                             //pointer 이동

    // printf("push!\n");
    
    if (ring_buf_pointer == RING_BUF_END) {         //버퍼 끝에 도달 시 pointer 0으로 초기화
        ring_buf_pointer = RING_BUF_START;
    }
}

uchar* Ring_buffer::Ring_Buffer_Pop(void)
{
    uchar *ret = 0;
    // printf("%d\n",ring_buf_pointer);              //debug용 현재 포인터 출력
    int previous=ring_buf_pointer-1;                 //이전 값은 현재 위치-1(한 칸 전의 데이터)

    if (ring_buf_pointer >= 1) {                     //링버퍼 안에 데이터 있을 시
        ret = ring_buf[previous].data;               //ret에 링버퍼에 저장되어있는 최신 데이터의 포인터 저장
        // printf("pop! \n");
    }
    else if(ring_buf_pointer == RING_BUF_START)      //끝에 도달하여 초기 칸으로 포인터가 돌아올 경우              
    {
        if(&ring_buf[previous].data != 0)            //초기에 push없이 pop하는거 방지용
        {
            ret = ring_buf[previous].data;           //ret에 링버퍼의 마지막에 저장되어있는 데이터의 포인터 저장
            // printf("last pop! \n");
        }
    }
    else                                             //데이터 없을경우 NULL 반환
    {
        // printf("no data\n");
        ret=NULL;
    }
    return ret;
}
 
Ring_buffer Ring;

void *cap_function(void *)                          //thread 함수
{
    cv::Mat frame;                                 
    cv::VideoCapture cap(CAM_NUM);
    
    while(1)                                        //계속 frame에 이미지 저장
    {
        pthread_mutex_lock(&mutx);
        if (!cap.isOpened())                        //카메라 연결 안 되어 있을 경우
        { 
        printf("카메라를 열수 없습니다. \n"); 
        }
        
        cap>>frame;
        
        Ring.Ring_Buffer_Push(frame);               //Ring buffer에 현재 이미지 넣음.
        pthread_mutex_unlock(&mutx);
    }
}

int main(void)
{ 
    int thr_id;                                     //thread 생성 확인 변수
    bool on_off=1;                                  //무한 반복문 탈출용 변수
    pthread_t thread;                               //thread 식별자
    cv::Mat frame2(1080,1920,CV_8UC3);              //height : 1080, weight : 1920, type : CV8UC3 - 8bit unsigned char color
    uchar *ptr;                                     //pop할 때 저장할 포인터 변수
    int state;
    state=pthread_mutex_init(&mutx,NULL);
    if(state)
    {
        perror("mutex init error");
        exit(EXIT_FAILURE);
    }
    
    thr_id = pthread_create(&thread, NULL, cap_function,NULL);  
                                                    //thread 생성 후 cap 함수 돌림
    if(thr_id < 0)                                  //thread 생성 실패 시
    {
        perror("thread create error");
        exit(EXIT_FAILURE);
    }

    while(on_off)                                   //esc 누르기 전 까지 무한 반복
    {
        ptr=Ring.Ring_Buffer_Pop();                 //ptr에 링버퍼에 저장된 이미지 주소 저장

        if(ptr!=NULL)                               //링버퍼에 데이터가 존재 할 경우에만 동작
        {
            memcpy(frame2.data,ptr,IMAGE_SIZE);     //프레임에 이미지 복사
            cv::imshow("camera1", frame2);          //출력
        }
        else
        {
            //do nothing
        }
        
        if (cv::waitKey(1) == 27)                   //esc 눌렀을 경우
        {
            on_off=0;                               //on_off 변수 0으로 만들어 무한반복 종료
        }
        else
        {
            //do nothing
        }
        usleep(30000);                              //30ms sleep 
    }
    pthread_mutex_destroy(&mutx); 
    close(thread);                                  //thread 종료
    return 0; 
}
