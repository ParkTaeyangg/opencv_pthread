#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include "opencv2/opencv.hpp"
#include "Ringbuffer2.h"
 
Ring_buffer Ring;

void *cap_function(void *)                          //thread 함수
{
    cv::Mat frame;                                 
    cv::VideoCapture cap(CAM_NUM);

    while(1)                                        //계속 frame에 이미지 저장
    {
        if (!cap.isOpened())                        //카메라 연결 안 되어 있을 경우
        { 
        printf("카메라를 열수 없습니다. \n"); 
        }
        
        cap>>frame;
        
        Ring.Ring_Buffer_Push(frame);               //Ring buffer에 현재 이미지 넣음.
    }
}

int main(void)
{ 
    int thr_id;                                     //thread 생성 확인 변수
    bool on_off=1;                                  //무한 반복문 탈출용 변수
    pthread_t thread;                               //thread 식별자
    cv::Mat frame2(1080,1920,CV_8UC3);              //height : 1080, weight : 1920, type : CV8UC3 - 8bit unsigned char color
    uchar *ptr;                                     //pop할 때 저장할 포인터 변수
    
    thr_id = pthread_create(&thread, NULL, cap_function,NULL);  
                                                    //thread 생성 후 cap 함수 돌림
    if(thr_id < 0)                                  //thread 생성 실패 시
    {
        perror("thread create error");
        exit(EXIT_FAILURE);
    }

    while(on_off)                                   //esc 누르기 전 까지 무한 반복
    {
        ptr=Ring.Ring_Buffer_Pop();                 //ptr에 링버퍼에 저장된 이미지 주소 저장

        if(ptr!=NULL)                               //링버퍼에 데이터가 존재 할 경우에만 동작
        {
            memcpy(frame2.data,ptr,IMAGE_SIZE);     //프레임에 이미지 복사
            cv::imshow("camera1", frame2);          //출력
        }
        else
        {
            //do nothing
        }
        
        if (cv::waitKey(1) == 27)                   //esc 눌렀을 경우
        {
            on_off=0;                               //on_off 변수 0으로 만들어 무한반복 종료
        }
        else
        {
            //do nothing
        }
        usleep(30000);                              //30ms sleep 
    } 
    close(thread);                          //thread 종료
    return 0; 
}